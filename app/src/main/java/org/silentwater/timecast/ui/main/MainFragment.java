package org.silentwater.timecast.ui.main;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.silentwater.timecast.R;

import java.util.concurrent.TimeUnit;

public class MainFragment extends Fragment {

    private long timeCountInMilliSeconds = 60000L;
    private final String REPS = "numOfRepetitionsPreference";
    private final String TAG_LOG = "timecast";

    private int duration;
    private int reps;

    private enum TimerStatus {
        STARTED,
        PAUSED,
        STOPPED
    }

    private TimerStatus timerStatus = TimerStatus.STOPPED;

    private ProgressBar progressBarCircle;
    private TextView editTextReps;
    private TextView textViewTime;
    private ImageView imageViewReset;
    private ImageView imageViewStartStop;
    private CountDownTimer countDownTimer;
    private SharedPreferences prefs;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = PreferenceManager.getDefaultSharedPreferences(requireContext());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // method call to initialize the views
        initViews();
        // method call to initialize the listeners
        initListeners();
        // set the initial time
        setTimerValues();
        // initialize the timer object
        initCountDownTimer(timeCountInMilliSeconds);
    }

    /**
     * method to initialize the views
     */
    @SuppressLint("SetTextI18n")
    private void initViews() {
        progressBarCircle = requireActivity().findViewById(R.id.progressBarCircle);
        reps = Integer.parseInt(prefs.getString(REPS, "5"));
        editTextReps = requireActivity().findViewById(R.id.editTextReps);
        editTextReps.setText(Integer.toString(reps));
        textViewTime = requireActivity().findViewById(R.id.textViewTime);
        String PAUSE_TIME = "pauseDurationPreference";
        String EXERCISE_TIME = "timeForExercisePreference";
        duration = Integer.parseInt(prefs.getString(PAUSE_TIME, "10")) +
                Integer.parseInt(prefs.getString(EXERCISE_TIME, "30"));
        @SuppressLint("DefaultLocale") String viewTime = String.format("%02d:%02d", duration / 60 , duration % 60);
        textViewTime.setText(viewTime);
        imageViewReset = requireActivity().findViewById(R.id.imageViewReset);
        imageViewStartStop = requireActivity().findViewById(R.id.imageViewStartStop);
    }

    /**
     * method to initialize the click listeners
     */
    private void initListeners() {
        imageViewReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.imageViewReset:
                        reset();
                        break;
                    case R.id.imageViewStartStop:
                        startStop();
                        break;
                }
            }
        });
        imageViewStartStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.imageViewReset:
                        reset();
                        break;
                    case R.id.imageViewStartStop:
                        startStop();
                        break;
                }
            }
        });
    }

    /**
     * Method that initialize the countDown timer object
     */
    private void initCountDownTimer(final long timeForCountDown)
    {
        countDownTimer = new CountDownTimer(timeForCountDown, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.d(TAG_LOG, "onTick called with " + (millisUntilFinished / 1000));
                textViewTime.setText(hmsTimeFormatter(millisUntilFinished));
                progressBarCircle.setProgress((int) (millisUntilFinished / 1000));
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onFinish() {
                Log.d(TAG_LOG, "onFinish called");
                // Checks how many reps are remaining
                if (reps > 0)
                {
                    // In this case, resets the timer to the beginning and edits the reps remaining
                    reps -= 1;
                    editTextReps.setText(Integer.toString(reps));
                    timerStatus = TimerStatus.STOPPED;

                    startStop();
                    return;
                }
                textViewTime.setText(hmsTimeFormatter(timeCountInMilliSeconds));
                // call to initialize the progress bar values
                setProgressBarValues();
                // hiding the reset icon
                imageViewReset.setVisibility(View.GONE);
                // changing stop icon to start icon
                imageViewStartStop.setImageResource(R.drawable.ic_play_arrow_white_48dp);
                // changing the timer status to stopped
                timerStatus = TimerStatus.STOPPED;
                // resets the reps
                reps = Integer.parseInt(prefs.getString(REPS, "5"));
                editTextReps.setText(Integer.toString(reps));
            }

        };
    }

    /**
     * method to reset count down timer
     */
    private void reset() {
        stopCountDownTimer();
        initViews();
        setTimerValues();
        setProgressBarValues();
        initCountDownTimer(timeCountInMilliSeconds);
        // hiding the reset icon
        imageViewReset.setVisibility(View.GONE);
        // changing stop icon to start icon
        imageViewStartStop.setImageResource(R.drawable.ic_play_arrow_white_48dp);
        timerStatus = TimerStatus.STOPPED;
    }

    /**
     * method to start and stop count down timer
     */
    private void startStop() {
        Log.d(TAG_LOG, "TimerStatus = " + timerStatus);
        // Timer is stopped, we need to start it
        if (timerStatus == TimerStatus.STOPPED) {
            // call to initialize the timer values
            setTimerValues();
            // call to initialize the progress bar values
            setProgressBarValues();
            // showing the reset icon
            imageViewReset.setVisibility(View.VISIBLE);
            // changing play icon to stop icon
            imageViewStartStop.setImageResource(R.drawable.ic_pause_white_48dp);
            // changing the timer status to started
            timerStatus = TimerStatus.STARTED;
            // call to start the count down timer
            startCountDownTimer();
        }
        // Timer is paused, we need to resume
        else if (timerStatus == TimerStatus.PAUSED) {
            // showing the reset icon
            imageViewReset.setVisibility(View.VISIBLE);
            // changing play icon to stop icon
            imageViewStartStop.setImageResource(R.drawable.ic_pause_white_48dp);
            // changing the timer status to started
            timerStatus = TimerStatus.STARTED;
            // call to start the count down timer
            startCountDownTimer();
        }
        // Timer is started, we need to pause it
        else {
            // hiding the reset icon
            imageViewReset.setVisibility(View.GONE);
            // changing stop icon to start icon
            imageViewStartStop.setImageResource(R.drawable.ic_play_arrow_white_48dp);
            // changing the timer status to paused
            timerStatus = TimerStatus.PAUSED;
            stopCountDownTimer();
        }

    }

    /**
     * method to set the values for count down timer
     */
    private void setTimerValues() {
        Log.d(TAG_LOG, "setTimerValues called");
        timeCountInMilliSeconds = duration * 1000;
    }

    /**
     * method to start count down timer
     */
    private void startCountDownTimer() { countDownTimer.start(); }

    /**
     * method to stop count down timer
     */
    private void stopCountDownTimer() {
        countDownTimer.cancel();
    }

    /**
     * method to set circular progress bar values
     */
    private void setProgressBarValues() {
        Log.d(TAG_LOG, "setProgressBarValues called");
        progressBarCircle.setMax((int) timeCountInMilliSeconds / 1000);
        progressBarCircle.setProgress((int) timeCountInMilliSeconds / 1000);
    }


    /**
     * method to convert millisecond to time format
     *
     * @param milliSeconds milliseconds to convert
     * @return HH:mm:ss time formatted string
     */
    @SuppressLint("DefaultLocale")
    private String hmsTimeFormatter(long milliSeconds) {
        return String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(milliSeconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliSeconds)),
                TimeUnit.MILLISECONDS.toSeconds(milliSeconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliSeconds)));
    }

}
