package org.silentwater.timecast.ui.main;

import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.preference.EditTextPreference;
import androidx.preference.PreferenceFragmentCompat;

import org.silentwater.timecast.R;


public class SettingsFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.settings, rootKey);

        // Gets all the edit text preferences and set them as integer inputs
        EditTextPreference repetitionsPreference = getPreferenceManager().findPreference("numOfRepetitionsPreference");
        EditTextPreference pauseDurationPreference = getPreferenceManager().findPreference("pauseDurationPreference");
        EditTextPreference timeForExercisePreference = getPreferenceManager().findPreference("timeForExercisePreference");


        androidx.preference.EditTextPreference.OnBindEditTextListener listener = new EditTextPreference.OnBindEditTextListener() {
            /**
             * Sets the edit text preferences to input only numbers
             */
            @Override
            public void onBindEditText(@NonNull EditText editText) {
                editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);

            }
        };

        repetitionsPreference.setOnBindEditTextListener(listener);
        pauseDurationPreference.setOnBindEditTextListener(listener);
        timeForExercisePreference.setOnBindEditTextListener(listener);

    }

}
